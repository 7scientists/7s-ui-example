import Settings from '7s/utils/settings'

import baseSettings from './_base'
import ssSettings from '7s/settings/prod'

const settings = new Settings()

settings.update(baseSettings)
settings.update(ssSettings)

export default settings