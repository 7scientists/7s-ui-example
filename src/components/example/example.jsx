import React from 'react'
import { A } from '7s/components'

const Example = () =>
    <div>
        <A href="/">
            Go back to main view...
            </A>
    </div>

export default Example
